package examplesoureit.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.enter_field)
    public EditText enterField;

    @BindView(R.id.result)
    public TextView resultView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.solve)
    void solve() {
        if (enterField.getText().toString().equals("")) {
            (Toast.makeText(this, "Write first  number", Toast.LENGTH_SHORT)).show();
        }

        String firstNumber, secondNumber;
        try {
            firstNumber = enterField.getText().toString().substring(0, findOperand());
            secondNumber = enterField.getText().toString().substring(findOperand() + 1, enterField.getText().toString().length());
            if (secondNumber.length() == 0) {
                (Toast.makeText(this, "Enter second number", Toast.LENGTH_SHORT)).show();
                return;
            }
            resultView.setText(calculate(firstNumber, secondNumber, choozenOperand()));
        } catch (StringIndexOutOfBoundsException e) {
            (Toast.makeText(this, "Choose an operand", Toast.LENGTH_SHORT)).show();
            return;
        }


    }

    @OnClick(R.id.clear)
    void clear() { //Чистит поле
        enterField.setText("");
    }

    @OnClick(R.id.addd)
    void clickAdd() {
        addSymbol("+");
    }

    @OnClick(R.id.deduct)
    void clickDeduct() {
        addSymbol("-");
    }

    @OnClick(R.id.mulltiply)
    void clickMultipy() {
        addSymbol("*");
    }

    @OnClick(R.id.divide)
    void clickDivide() {
        addSymbol("/");
    }

    public int findOperand() {
        String[] operands = {"+", "-", "*", "/"};
        int indexOfOperand;
        for (String operand : operands) {
            if (enterField.getText().toString().contains(operand)) {
                indexOfOperand = enterField.getText().toString().indexOf(operand);
                return indexOfOperand;
            }
        }
        return -1;
    }

    public String choozenOperand() {
        String[] operands = {"+", "-", "*", "/"};
        for (String operand : operands) {
            if (enterField.getText().toString().contains(operand)) {
                return operand;
            }
        }
        return "none";
    }

    public void addSymbol(String s) {
        if (enterField.getText().toString().equals("")) {
            (Toast.makeText(this, "Enter number first", Toast.LENGTH_SHORT)).show();
            return;
        }

        if (findOperand() < 0) {
            enterField.setText(enterField.getText().toString().concat(s));
        } else {
            enterField.setText(enterField.getText().toString().replace(choozenOperand(), s));
        }
        enterField.setSelection(enterField.getText().toString().length());
    }

    public String calculate(String firstNumber, String secondNumber, String operand) {
        String result;
        int first = Integer.parseInt(firstNumber);
        int second = Integer.parseInt(secondNumber);

        switch (operand) {
            case "+":
                result = String.valueOf(first + second);
                break;
            case "-":
                result = String.valueOf(first - second);
                break;
            case "*":
                result = String.valueOf(first * second);
                break;
            case "/":
                result = String.valueOf((float) first / (float) second);
                break;
            default:
                result = "error";
        }
        return result;
    }


}
